Car Color Detection Using Tensorflow and OpenCV

Installations Required : 

 TensorFlow version > 1.4.0,
 TensorFlow Object Detection API - Models Master,
 OpenCV,
 Pillow,
 scikit,
 Jupyter Notebook,
 Python.
 
Assuming all the above are installed :

 Load the "main.ipynb" onto jupyter Notebook and check all the modules are imported or not.
 Make sure the proper file path is provided for the image.
 
 Once you run the .ipynb file the image ,imageWindow will be opened with the bounding box with the car name on top of it with probability percentage.
 
 WordCount.java is map reduce program to be run on Hadoop to reduce the final output and give the final output.
 
1-> This uses tensorflow and the object prediction API ,so the detection level or probability is very high.

2-> The object detection model has been trained by over 300k+ images so it is very accurate.

3-> The color detection detects all the "shades of the color" and then they are mapped to the primary color.All the shades of the color are accepted.

4-> We have used Hadoop map - reduce program to reduce the data because it splits the data thereby reducing the calculation time.

5-> It considers only the car objects and the accuracy is very high.

6-> The detection rate or speed of detection is very high and the bounding box drawn is very accurate.

